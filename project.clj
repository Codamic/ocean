(defproject codamic/ocean "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure    "1.9.0"]
                 [codamic/hellhound.core "1.0.0-SNAPSHOT"]
                 [codamic/hellhound.http "1.0.0-SNAPSHOT"]]

  :aliases {"fig" ["trampoline" "run" "-m" "figwheel.main"]
            "build-dev" ["trampoline" "run" "-m" "figwheel.main" "-b" "dev" "-r"]}

  :resource-paths ["resources"]
  :source-paths ["src/clj" "src/cljs"]
  :main ^:skip-aot ocean.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
