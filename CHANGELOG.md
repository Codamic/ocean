# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## [Unreleased]
### Changed
- Documentation on how to make the widgets.

### Removed
- `make-widget-sync` - we're all async, all the time.

### Fixed
- Fixed widget maker to keep working when daylight savings switches over.

[Unreleased]: https://gitlab.com/Codamic/ocean/compare/0.1.1...HEAD
[0.1.1]: https://gitlab.com/Codamic/ocean/compare/0.1.0...0.1.1
