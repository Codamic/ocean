;; Ocean - Stock market analyzer
;; Copyright (C) 2017-2019  Codamic Technologies

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
(ns tech.codamic.ocean.core
  (:gen-class)
  (:require
   [tech.codamic.ocean.system :as system]
   [hellhound.system :as sys]))


(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!")
  (sys/set-system! system/dev)
  (sys/start!))
